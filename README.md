# Blue Harvest Technical Assignment

## Instructions

* Run `npm install` to install all local npm dev dependencies
* Run `npm run start` to start the dev server.
* Navigate to `http://localhost:9090/`.
