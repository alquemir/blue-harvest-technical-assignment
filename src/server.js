const axios = require('axios');
const express = require('express');

const server = express();
const httpPort = 9090;
const url = 'http://api.nytimes.com/svc/search/v2/articlesearch.json?q=amsterdam&api-key=b75da00e12d54774a2d362adddcc9bef';

server.get('/ping', (request, response) => response.send('PONG ...'));
server.get('/articles', (request, response) => {
  axios.request(url).then((resp) => {
    response.json(resp.data.response.docs);
  });
});

server.listen(httpPort, () => { console.log(`LISTENING ON PORT ${httpPort} ...`); });
