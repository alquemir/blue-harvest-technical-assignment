import { combineReducers, applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import articles from './reducers/articles';
import modal from './reducers/modal';

export default function createReduxStore() {
  return createStore(
    combineReducers({ articles, modal }),
    applyMiddleware(thunkMiddleware, loggerMiddleware)
  );
}
