import { getFilteredArticles } from '../../selectors';

const getInitialState = () => ({
  articles: {
    articles: [
      {
        web_url: 'https://www.nytimes.com/travel/guides/europe/netherlands/amsterdam/overview.html',
        snippet: 'Articles, photos and video about Amsterdam from The New York Times, including hotel, restaurant and attraction information with reader reviews and advice on where to stay, where to eat and what to do.',
        blog: {},
        multimedia: [],
        headline: {
          main: 'Amsterdam',
          kicker: null,
          content_kicker: null,
          print_headline: null,
          name: null,
          seo: null,
          sub: null
        },
        keywords: [],
        document_type: 'topic',
        type_of_material: 'traveltopic',
        _id: '52bd909738f0d8377a7ad74c',
        word_count: 33,
        score: 1.2688055
      }, {
        web_url: 'https://travel.nytimes.com/travel/guides/europe/netherlands/amsterdam/overview.html',
        snippet: 'Articles, photos and video about Amsterdam from The New York Times, including hotel, restaurant and attraction information with reader reviews and advice on where to stay, where to eat and what to do.',
        blog: {},
        multimedia: [],
        headline: {
          main: 'Amsterdam',
          kicker: null,
          content_kicker: null,
          print_headline: null,
          name: null,
          seo: null,
          sub: null
        },
        keywords: [],
        document_type: 'topic',
        type_of_material: 'traveltopic',
        _id: '527aacf238f0d8660663194b',
        word_count: 33,
        score: 1.2479855
      }, {
        web_url: 'https://www.nytimes.com/2018/03/26/arts/design/vincent-van-gogh-japan.html',
        snippet: 'After buying some prints in Paris in 1886, the Dutch painter became obsessed by Japanese art. An exhibition in Amsterdam explores how that fascination shaped his work.',
        print_page: '2',
        blog: {},
        source: 'The New York Times',
        multimedia: [],
        headline: {},
        keywords: [],
        pub_date: '2018-03-26T06:00:09+0000',
        document_type: 'article',
        new_desk: 'Culture',
        section_name: 'Art & Design',
        byline: {},
        type_of_material: 'News',
        _id: '5ab88c6e068401528a29cc4c',
        word_count: 1135,
        score: 0.7844611,
        uri: 'nyt://article/6e2c1a45-ab96-5007-884e-c4752a34268e'
      }, {
        web_url: 'https://www.nytimes.com/2018/04/12/world/europe/netherlands-singing-road.html',
        snippet: 'Officials in the Netherlands removed musical strips painted on a highway after people living nearby said they couldn’t sleep.',
        print_page: '13',
        blog: {},
        source: 'The New York Times',
        multimedia: [],
        headline: {},
        keywords: [],
        pub_date: '2018-04-12T16:00:21+0000',
        document_type: 'article',
        new_desk: 'Foreign',
        section_name: 'Europe',
        byline: {},
        type_of_material: 'News',
        _id: '5acf8298068401528a2a824c',
        word_count: 909,
        score: 0.72070676,
        uri: 'nyt://article/570b40f2-5a45-5492-af79-a4db21510ef8'
      }
    ],
    searchTerm: '',
    isLoading: false
  }
});


describe('articles selectors', () => {
  
  
  it('should return all articles if an empty search term is passed', () => {
    const state = getInitialState(); 
    state.articles.searchTerm = '';
    expect(getFilteredArticles(state).length).toEqual(4);
  });

  it('should find articles by \'type_of_material\' field, using standard search term', () => {
    const state = getInitialState(); 
    state.articles.searchTerm = 'News';
    expect(getFilteredArticles(state).length).toEqual(2);
  });
  
   
  it('should find articles by \'type_of_material\' field, using lower case search term', () => {
    const state = getInitialState(); 
    state.articles.searchTerm = 'news';
    expect(getFilteredArticles(state).length).toEqual(2);
  });

  it('should find articles by \'type_of_material\' field, using partial search term', () => {
    const state = getInitialState(); 
    state.articles.searchTerm = 'ne';
    expect(getFilteredArticles(state).length).toEqual(2);
  });
  
});




