import { createSelector } from 'reselect';
import { isEmptyString } from '../common';

const getArticles = state => state.articles.articles;
const getSearchTerm = state => state.articles.searchTerm;
const getSelectedArticleId = state => state.articles.articleId;
const isSelectedArticle = (article, articleId) => article._id === articleId;
const matchesSearchTerm = (article, searchTerm) => {
  const input = article.type_of_material;
  return input.toLowerCase().startsWith(searchTerm.toLowerCase());
};

export const getFilteredArticles = createSelector(
  [getArticles, getSearchTerm],
  (articles, searchTerm) => { 
  return !isEmptyString(searchTerm)
    ? articles.filter(article => matchesSearchTerm(article, searchTerm))
    : articles
  }
);

export const getSelectedArticle = createSelector(
  [getArticles, getSelectedArticleId],
  (articles, articleId) => articles.find(article => isSelectedArticle(article, articleId))
);

