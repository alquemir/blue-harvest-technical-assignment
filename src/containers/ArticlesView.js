import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchArticles, selectArticle, filterArticles } from '../actions';
import { getSelectedArticle, getFilteredArticles } from '../selectors';
import ArticlesList from '../components/ArticlesList';
import SearchBox from '../components/SearchBox'; 

class ArticlesView extends Component {
  componentDidMount() {
    this.props.fetchArticles();
  }

  handleArticleClick(articleId) {
    this.props.selectArticle(articleId);
  }

  handleArticleSearch(searchTerm) {
    this.props.filterArticles(searchTerm);
  }

  render() {
    console.log(this.props);
    return (
      <div className="container">
        <SearchBox onSearch={searchTerm => this.handleArticleSearch(searchTerm)} />
        <ArticlesList 
          onArticleClick={articleId => this.handleArticleClick(articleId)} 
          selectedArticle={this.props.selectArticle}
          onArticleSearch={searchTerm => this.handleArticleSearch(searchTerm)}
          articles={this.props.articles} 
        />
        <div></div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchArticles,
  selectArticle,
  filterArticles
}, dispatch);

const mapStateToProps = state => ({
  articles: getFilteredArticles(state),
  selectedArticle: getSelectedArticle(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesView);
