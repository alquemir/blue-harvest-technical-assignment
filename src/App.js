import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

import ArticlesView from './containers/ArticlesView';

const App = () => (
  <div className="app">
    <main>
      <ArticlesView />
    </main>
  </div>
);


export default App;
