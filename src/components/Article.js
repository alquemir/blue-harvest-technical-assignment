import React from 'react';
import { invokeFunction } from '../common';

const Article = ({
  id,
  headline,
  snippet,
  onClick
}) => {
  const handleClick = () => {
    invokeFunction(onClick, id);
  };

  const renderHeadline = () => (
    <div className="row no-gutters">
      <div className="col-12">
        <span className="headline">{headline}</span>
      </div>
    </div>
  );

  const renderSnippet = () => (
    <div className="row no-gutters">
      <div className="col-12">
        <p className="snippet">{snippet}</p>
      </div>
    </div>
  );

  return (
    <div onClick={handleClick} className="article">
      {renderHeadline()}
      {renderSnippet()}
    </div>
  );
};

export default Article;
