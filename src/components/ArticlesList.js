import React from 'react';
import { isEmptyArray, invokeFunction } from '../common';
import Article from './Article';

const ArticlesList = ({
  articles,
  onArticleClick
}) => {
  const handleArticleClick = (articleId) => {
    invokeFunction(onArticleClick, articleId);
  };

  const hasArticles = () => isEmptyArray(articles);

  const getArticleKey = index => `article-${index + 1}`;

  const isSelectedArticle = article => this.props.isSelected === true;

  const renderArticles = () => articles.map((row, index) => (
    <Article
      key={getArticleKey(index)}
      id={row._id}
      headline={row.headline.main}
      isSelected={isSelectedArticle(article)}
      snippet={row.snippet}
      url={row.web_url}
      onClick={handleArticleClick}
    />
  ));

  return (
    <div className="articles-list">
      {hasArticles ? renderArticles() : null}
    </div>
  );
};

export default ArticlesList;

