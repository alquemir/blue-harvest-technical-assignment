import React from 'react';
import { invokeFunction } from '../common';

const SearchBox = ({ onSearch, placeholder }) => {
  const handleSearch = (event) => {
    invokeFunction(onSearch, event.target.value);
  };

  const getPlaceholder = () => placeholder || 'Search ...';

  const renderSeachTextbox = () => (
    <input
      type="text"
      onChange={handleSearch}
      className="form-control"
      placeholder={getPlaceholder()}
      aria-label={getPlaceholder()}
      aria-describedby="basic-addon2"
    />
  );

  const renderSearchButton = () => (
    <div className="input-group-append">
      <button className="btn btn-outline-secondary" type="button">Button</button>
    </div>
  );

  return (
    <div className="search-box">
      <div className="row no-gutters">
        <div className="input-group">
          {renderSeachTextbox()}
          {renderSearchButton()}
        </div>
      </div>
    </div>
  );
};

export default SearchBox;
