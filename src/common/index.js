import * as R from 'ramda';

export const isString = input => R.is(String, input);
export const isFunction = fn => R.is(Function, fn);
export const isArray = input => R.is(Array, input);
export const noop = () => {};
export const isEmptyArray = input => isArray(input) && input.length === 0;
export const isEmptyString = input => isString(input) && input.length === 0;
export const invokeFunction = (fn, ...args) => (isFunction(fn) ? fn(...args) : noop());
export const equalArrays = (a, b) => R.allPass([R.equals, isArray])(a, b);
