import * as R from 'ramda';
import ActionTypes from '../actions/ActionTypes';

const initialState = {
  isOpen: false
};

export default function modal(state = initialState, action) {
  return R.cond([
    [
      () => action.type === ActionTypes.OPEN_MODAL,
      () => ({ ...state, isOpen: true })
    ],
    [
      () => action.type === ActionTypes.CLOSE_MODAL,
      () => ({ ...state, isOpen: false })
    ],
    [
      R.T,
      () => state
    ]
  ])();
}
