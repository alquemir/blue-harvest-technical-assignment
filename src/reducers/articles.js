import * as R from 'ramda';
import ActionTypes from '../actions/ActionTypes';

const initialState = {
  articles: [],
  isLoading: false,
  articleId: undefined,
  searchTerm: ''
};

const articles = (state = initialState, action) => R.cond([
  [
    () => action.type === ActionTypes.SELECT_ARTICLE,
    () => ({ ...state, articleId: action.articleId })
  ],
  [
    () => action.type === ActionTypes.RECEIVE_ARTICLES,
    () => ({ ...state, articles: action.articles })
  ],
  [
    () => action.type === ActionTypes.FILTER_ARTICLES,
    () => ({ ...state, searchTerm: action.searchTerm })
  ],
  [
    R.T,
    () => state
  ]
])();

export default articles;
