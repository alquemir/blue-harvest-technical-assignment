import ActionTypes from './ActionTypes';

export const filterArticles = searchTerm => ({
  type: ActionTypes.FILTER_ARTICLES,
  searchTerm
});

export const selectArticle = articleId => ({
  type: ActionTypes.SELECT_ARTICLE,
  articleId
});

export const receiveArticles = articles => ({
  type: ActionTypes.RECEIVE_ARTICLES,
  articles
});

export const fetchArticles = () =>
  dispatch => fetch('/articles')
    .then(response => response.json())
    .then(json => dispatch(receiveArticles(json)));

